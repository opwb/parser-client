<?php

namespace Client\Api\Responses\Handlers;

class Basic extends Handler
{
    public function handle($response)
    {
        return json_decode($response, true);
    }
}
