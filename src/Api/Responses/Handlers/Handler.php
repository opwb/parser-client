<?php

namespace Client\Api\Responses\Handlers;

abstract class Handler
{
    abstract public function handle($response);
}
