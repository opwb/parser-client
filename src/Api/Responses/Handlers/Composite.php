<?php

namespace Client\Api\Responses\Handlers;

class Composite extends Basic
{
    /**
     * @param array $response
     *
     * @return bool
     */
    public function isEmpty($response)
    {
        return array_has($response, 'data.entries') && count($response['data']['entries']) === 0;
    }

    /**
     * @param array $response
     * @param int|null $limit
     *
     * @return bool
     */
    public function limitReached($response, $limit)
    {
        if ($limit === null || empty($response)) {
            return false;
        }

        return count($response['data']['entries']) >= $limit;
    }

    /**
     * @param array $previousResponse
     * @param array $currentResponse
     * @param int|null $limit
     *
     * @return array
     */
    public function mergeResponses($previousResponse, $currentResponse, $limit = null)
    {
        // todo change increment offset, count

        if (empty($previousResponse)) {
            return $currentResponse;
        }

        $response = $previousResponse;
        $entries = array_get($currentResponse, 'data.entries');
        if (is_array($entries) && count($entries) > 0) {
            $response['data']['entries'] = array_merge_recursive($previousResponse['data']['entries'], $currentResponse['data']['entries']);
        }

        return $response;
    }

    /**
     * @param array $response
     * @param int|null $limit
     *
     * @return array
     */
    public function limitResponse($response, $limit = null)
    {
        if ($limit === null) {
            return $response;
        }

        if ($this->limitReached($response, $limit)) {
            $response['data']['entries'] = array_slice($response['data']['entries'], 0, $limit, true);
        }

        return $response;
    }
}
