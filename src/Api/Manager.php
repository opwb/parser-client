<?php

namespace Client\Api;

use Client\Api\Engines\Engine;
use Client\Api\Engines\ContinuousGet as ContinuousGetEngine;
use Client\Api\Engines\Get as GetEngine;
use Client\Api\Engines\Post as PostEngine;
use Client\Api\Requests\Request;
use Client\Api\Requests\Messages;
use Client\Api\Requests\Categories;
use Client\Api\Requests\Channels;
use Client\Api\Requests\ChannelMessages;
use Client\Api\Responses\Handlers\Basic as BasicResponseHandler;
use Client\Api\Responses\Handlers\Composite as CompositeResponseHandler;
use Client\Api\Responses\Response;
use Generator;

class Manager
{
    /**
     * @var Provider
     */
    private $provider;

    /**
     * @var array
     */
    private static $methods = [
        Messages::TYPE => [
            'method' => 'message',
            'type' => 'continuous_get',
        ],
        Categories::TYPE => [
            'method' => 'category',
            'type' => 'continuous_get',
        ],
        Channels::TYPE => [
            'method' => 'channel',
            'type' => 'continuous_get',
        ],
        ChannelMessages::TYPE => [
            'method' => 'message/channel',
            'type' => 'continuous_get',
        ],
    ];

    /**
     * @param Provider $provider
     */
    public function __construct(Provider $provider)
    {
        $this->provider = $provider;
    }

    /**
     * @param Request $request
     * @param int|null        $limit
     *
     * @return Response
     */
    public function process(Request $request, $limit = null)
    {
        $type = $request->getType();
        if (!array_key_exists($type, self::$methods)) {
//            throw new Exception('Can`t process request: undefined type ' . $type);
        }

        $engine = $this->getEngine(self::$methods[$type])
            ->setMethod(self::$methods[$type]['method'])
            ->setData($request->getData());
        if ($limit !== null) {
            $engine->setLimit($limit);
        }

        $response = $this->provider->process($engine);

//        if (isset($response['status']) && $response['status'] === 'error') {
//            throw new Exception('Error response with message: "' . $response['message'] . '"');
//        }

        if ($response instanceof Generator) {
            foreach ($response as $item) {
                yield new Response($item);
            }
        } elseif (is_array($response)) {
            yield new Response($response);
        }
    }

    /**
     * @param array $method
     *
     * @return Engine|null
     */
    private function getEngine(array $method)
    {
        $engine = null;

        switch ($method['type']) {
            case 'get':
                $engine = new GetEngine(new BasicResponseHandler());
                break;
            case 'continuous_get':
                $engine = new ContinuousGetEngine(new CompositeResponseHandler());
                break;
            case 'post':
                $engine = new PostEngine(new BasicResponseHandler());
                break;
        }

        return $engine;
    }
}
