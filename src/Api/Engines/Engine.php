<?php

namespace Client\Api\Engines;

use Client\Api\Responses\Handlers\Handler;
use Client\Services\HttpClient\HttpClient;
use Client\Services\HttpClient\HttpClientException;
use Client\Services\HttpClient\TooManyRequestsException;

abstract class Engine
{
    /**
     * @var Handler
     */
    protected $responseHandler;

    /**
     * @var string
     */
    protected $method;

    /**
     * @var mixed
     */
    protected $data;

    /**
     * @var HttpClient
     */
    protected $httpClient;

    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @var string
     */
    protected $token;

    /** @var int|null */
    protected $limit;

    /**
     * @param Handler $responseHandler
     */
    public function __construct(Handler $responseHandler)
    {
        $this->responseHandler = $responseHandler;
    }

    /**
     * @param string $method
     *
     * @return self
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * @param mixed $data
     *
     * @return self
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @param HttpClient $httpClient
     *
     * @return self
     */
    public function setHttpClient($httpClient)
    {
        $this->httpClient = $httpClient;

        return $this;
    }

    /**
     * @param string $baseUrl
     *
     * @return self
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;

        return $this;
    }

    /**
     * @param int|null $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return HttpClient
     */
    public function getHttpClient()
    {
        return $this->httpClient;
    }

    /**
     * @return bool
     */
    public function hasErrors()
    {
        return false;
        //        return !$this->httpClient->isOk();
    }

    abstract public function request();

    public function response()
    {
        try {
            REQUEST:
            $response = config('app.env') === 'local'
                ? $this->httpClient->getAndCache()
                : $this->httpClient->get();

            return $this->responseHandler->handle($response);
        } catch (TooManyRequestsException $exception) {
            sleep(60);
            dump('too many');
            goto REQUEST;
        } catch (HttpClientException $exception) {
            dump($exception->getMessage());
        }
    }
}
