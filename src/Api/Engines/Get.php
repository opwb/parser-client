<?php

namespace Client\Api\Engines;

class Get extends Engine
{
    public function request()
    {
        $this->httpClient->setUrl($this->getUrl());
    }

    /**
     * @return string
     */
    protected function getUrl()
    {
        return $this->getBaseUrl() . $this->getData();
    }

    /**
     * @return string
     */
    protected function getBaseUrl()
    {
        return $this->baseUrl . $this->method;
    }

    protected function getData()
    {
        return $this->data;
    }
}
