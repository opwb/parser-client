<?php

namespace Client\Api\Engines;

class Post extends Engine
{
    public function request()
    {
        $this->httpClient->setMethod('POST');
        $this->httpClient->setUrl($this->getUrl());
        $this->httpClient->setJsonData($this->getData());
    }

    /**
     * @return string
     */
    protected function getUrl()
    {
        return $this->getBaseUrl();
    }

    /**
     * @return string
     */
    protected function getBaseUrl()
    {
        return $this->baseUrl . $this->method;
    }

    protected function getData()
    {
        return $this->data;
    }
}
