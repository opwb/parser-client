<?php

namespace Client\Api;

use Client\Api\Engines\Engine;
use Client\Services\HttpClient\HttpClientException;
use Client\Services\HttpClient\HttpClient;

class Provider
{
    private $host;
    private $version = '1.0.0';

    /**
     * @var HttpClient
     */
    private $httpClient;

    /**
     * @param HttpClient $httpClient
     * @param string $token
     * @param $host
     */
    public function __construct(HttpClient $httpClient, $token, $host)
    {
        $this->httpClient = $httpClient;
        $this->httpClient->setBearerAuthorization($token);
        $this->host = $host;
    }

    /**
     * @param Engine $engine
     *
     * @return array
     * @throws HttpClientException
     */
    public function process(Engine $engine)
    {
        $engine
            ->setHttpClient($this->httpClient)
            ->setBaseUrl($this->getUrl())
            ->request();

        return $engine->response();
    }

    /**
     * @return string
     */
    private function getUrl()
    {
        return "http://$this->host/$this->version/";
    }
}
