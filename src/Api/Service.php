<?php

namespace Client\Api;

use Client\Api\Entities\Message;
use Client\Api\Entities\Messages;
use Client\Api\Entities\Category;
use Client\Api\Entities\Categories;
use Client\Api\Entities\Channel;
use Client\Api\Entities\Channels;
use Client\Api\Requests\Request;
use Client\Api\Requests\Messages as MessagesRequest;
use Client\Api\Requests\ChannelMessages as ChannelMessagesRequest;
use Client\Api\Requests\Channels as ChannelsRequest;
use Client\Api\Requests\Categories as CategoriesRequest;

class Service
{
    /**
     * @var Manager
     */
    private $manager;

    /**
     * @param Manager $manager
     */
    public function __construct($manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param int $startId
     *
     * @return \Generator|Messages|Message[]
     */
    public function getMessages(int $startId = 0)
    {
       $request = new MessagesRequest;
        $request->setStartId($startId);

        foreach ($this->process($request) as $data) {
            $messages = (new Messages())->setData($data);
            foreach ($messages as $message) {
                yield $message;
            }
        }
    }

    public function getCategories(int $startId = 0)
    {
        $request = new CategoriesRequest;
        $request->setStartId($startId);

        foreach ($this->process($request) as $data) {
            $categories = (new Categories())->setData($data);
            foreach ($categories as $category) {
                yield $category;
            }
        }
    }

    public function getChannels(int $startId = 0)
    {
        $request = new ChannelsRequest;
        $request->setStartId($startId);

        foreach ($this->process($request) as $data) {
            $channels = (new Channels())->setData($data);
            foreach ($channels as $channel) {
                yield $channel;
            }
        }
    }

    public function getChannelMsg (int $startId = 0, int $channelId)
    {
        $request = new ChannelMessagesRequest;
        $request->setStartId($startId);
        $request->setChannelId($channelId);

        foreach ($this->process($request) as $data) {
            $messages = (new Messages())->setData($data);
            foreach ($messages as $message) {
                yield $message;
            }
        }
    }
    /**
     * @param Request $request
     * @param int|null $limit
     *
     * @return array
     */
    private function process(Request $request, $limit = null)
    {
        foreach ($this->manager->process($request) as $item) {
            yield $item->getData();
        }

        return [];
    }
}
