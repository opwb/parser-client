<?php declare(strict_types=1);

namespace Client\Api\Entities;

use Illuminate\Contracts\Support\Arrayable;
use ReflectionClass;

abstract class Entity implements Arrayable
{
    /**
     * @param array $data
     */
    abstract public function setData(array $data);

    /**
     * @return mixed
     */
    public function toArray()
    {
        $class = new ReflectionClass(static::class);
        $properties = array_merge($class->getParentClass()->getProperties(), $class->getProperties());
        $assoc = [];

        foreach ($properties as $property) {
            $value = call_user_func_array([$this, 'get'.ucfirst($property->name)], []);

            if (is_object($value) && in_array(Arrayable::class, class_implements($value))) {
                $assoc[$property->name] = $value->toArray();
            } else {
                $assoc[$property->name] = $value;
            }

        }

        return $assoc;
    }
}
