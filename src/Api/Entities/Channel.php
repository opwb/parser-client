<?php

declare(strict_types=1);

namespace Client\Api\Entities;

use Illuminate\Support\Arr;

class Channel extends Entity
{
    /** @var int */
    private $id;

    /** @var string */
    private $nick;

    /** @var string */
    private $name;

    /** @var array */
    private $categories;

    /**
     * @param  array $data
     *
     * @return self
     */
    public function setData(array $data): self
    {
        $this->id = (int)array_get($data, 'id');
        $this->nick = array_get($data, 'nick', '');
        $this->name = array_get($data, 'name', '');
        $this->categories = (array)array_get($data, 'categories_ids');

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNick(): ?string
    {
        return $this->nick;
    }


    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCategories(): array
    {
        return $this->categories;
    }
}
