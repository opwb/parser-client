<?php

declare(strict_types=1);

namespace Client\Api\Entities;

use Carbon\Carbon;

class Category extends Entity
{
    /** @var int */
    private $id;

    /** @var string */
    private $name;

    /**
     * @param  array $data
     *
     * @return self
     */
    public function setData(array $data): self
    {
        $this->id = (int)array_get($data, 'id');
        $this->name = array_get($data, 'name', '');

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

}
