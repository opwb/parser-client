<?php

declare(strict_types=1);

namespace Client\Api\Entities;

use Illuminate\Support\Collection;

class Categories extends Collection
{
    /**
     * @param array $messages
     *
     * @return self
     */
    public function setData(array $categories): self
    {
        $categories = array_get($categories, 'data.entries');
        if ($categories === null) {
            return $this;
        }

        foreach ($categories as $data) {
            $category = new Category();
            $this->push($category->setData($data));
        }

        return $this;
    }
}
