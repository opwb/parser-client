<?php

declare(strict_types=1);

namespace Client\Api\Entities;

use Illuminate\Support\Collection;

class Channels extends Collection
{
    /**
     * @param array $messages
     *
     * @return self
     */
    public function setData(array $channels): self
    {
        $channels = array_get($channels, 'data.entries');
        if ($channels === null) {
            return $this;
        }

        foreach ($channels as $data) {
            $channel = new Channel();
            $this->push($channel->setData($data));
        }

        return $this;
    }
}
