<?php

declare(strict_types=1);

namespace Client\Api\Entities;

use Illuminate\Support\Collection;

class Messages extends Collection
{
    /**
     * @param array $messages
     *
     * @return self
     */
    public function setData(array $messages): self
    {
        $messages = array_get($messages, 'data.entries');
        if ($messages === null) {
            return $this;
        }

        foreach ($messages as $data) {
            $message = new Message();
            $this->push($message->setData($data));
        }

        return $this;
    }
}
