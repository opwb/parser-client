<?php

declare(strict_types=1);

namespace Client\Api\Entities;

use Carbon\Carbon;

class Message extends Entity
{
    /** @var int */
    private $id;

    /** @var string */
    private $text;

    /** @var Carbon */
    private $date;

    /**
     * @param  array $data
     *
     * @return self
     */
    public function setData(array $data): self
    {
        $this->id = (int)array_get($data, 'id');
        $this->text = array_get($data, 'text', '');
        $this->date = new Carbon(array_get($data, 'date'));

        // todo 'categories'

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @return Carbon
     */
    public function getDate(): Carbon
    {
        return $this->date;
    }
}
