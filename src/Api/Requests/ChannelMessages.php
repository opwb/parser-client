<?php

declare(strict_types=1);

namespace Client\Api\Requests;

class ChannelMessages extends Request
{
    public const TYPE = 'message/channel/';

    /** @var int */
    private $startId;
    private $channelId;

    /**
     * @param int $startId
     */
    public function setStartId(int $startId): void
    {
        $this->startId = $startId;
    }

    public function setChannelId(int $channelId): void
    {
        $this->channelId = $channelId;
    }

    public function getData(): string
    {
        return parent::getData() . '/'.$this->channelId.'?start_id=' . $this->startId;
    }
}
