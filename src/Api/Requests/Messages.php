<?php

declare(strict_types=1);

namespace Client\Api\Requests;

class Messages extends Request
{
    public const TYPE = 'messages';

    /** @var int */
    private $startId;

    /**
     * @param int $startId
     */
    public function setStartId(int $startId): void
    {
        $this->startId = $startId;
    }

    public function getData(): string
    {
        return parent::getData() . '?start_id=' . $this->startId;
    }
}
