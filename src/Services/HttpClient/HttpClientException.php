<?php declare(strict_types=1);

namespace Client\Services\HttpClient;

use RuntimeException;

class HttpClientException extends RuntimeException
{
}
