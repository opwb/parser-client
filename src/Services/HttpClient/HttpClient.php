<?php declare(strict_types=1);

namespace Client\Services\HttpClient;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\SessionCookieJar;
use GuzzleHttp\Exception\BadResponseException;
use RuntimeException;

class HttpClient
{
    private const HTTP_CODE_TOO_MANY_REQUESTS = 429;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var array
     */
    protected $params;

    private $userAgent = '';

    private $headers = [];

    private $url;

    private $method = 'GET';

    private $jsonData = [];

    public function setUserAgent(string $userAgent)
    {
        $this->userAgent = $userAgent;
    }

    public function setHeader(string $key, string $value)
    {
        $this->headers[$key] = $value;
    }

    public function setUrl(string $url)
    {
        $this->url = $url;
    }

    public function setMethod(string $method): void
    {
        $this->method = $method;
    }

    public function setJsonData(array $data): void
    {
        $this->jsonData = $data;
    }

    public function setBearerAuthorization(string $token): void
    {
        $this->headers['Authorization'] = 'Bearer ' . $token;
    }

    /**
     * @param string $url
     * @param string|null $cacheName
     * @param string|null $cacheExt
     *
     * @return string
     */
    public function getAndCache(string $url = null, string $cacheExt = null, string $cacheName = null): string
    {
        if ($url === null) {
            $url = $this->url;
        }

        if ($url === null) {
            throw new HttpClientException('Empty url.');
        }

        if ($cacheName === null) {
            $cacheName = md5(str_slug($url) . serialize($this->jsonData));
        }

        if ($cacheExt !== null) {
            $cacheName .= '.' . $cacheExt;
        }

        $cacheFile = storage_path() . '/cache/' . $cacheName;

        if (is_readable($cacheFile)) {
            return file_get_contents($cacheFile);
        }

        $data = [
            'headers' => $this->headers,
        ];
        if (!empty($this->jsonData)) {
            $data['json'] = $this->jsonData;
        }

        try {
            $response = $this->getClient()->request('GET', $url, $data);
            $responseString = $response->getBody()->getContents();
        } catch (BadResponseException $exception) {
            throw new HttpClientException('Bad response.', 0, $exception);
        } catch (RuntimeException $exception) {
            throw new HttpClientException('HttpClient runtime error.', 0, $exception);
        }

        $cacheDirectory = pathinfo($cacheFile, PATHINFO_DIRNAME);
        if (@mkdir($cacheDirectory, 0755, true) && !is_dir($cacheDirectory)) {
            throw new HttpClientException('Can`t create directory for cache file.');
        }
        file_put_contents($cacheFile, $responseString);

        return $responseString;
    }

    /**
     * @param string $url
     *
     * @return string
     * @throws HttpClientException
     */
    public function get(string $url = null): string
    {
        if ($url === null) {
            $url = $this->url;
        }

        if ($url === null) {
            throw new HttpClientException('Empty url.');
        }

        try {
            $data = [
                'headers' => $this->headers,
            ];
            if (!empty($this->jsonData)) {
                $data['json'] = $this->jsonData;
            }
            $response = $this->getClient()->request($this->method, $url, $data);
            $responseString = $response->getBody()->getContents();
        } catch (BadResponseException $exception) {
            if ($exception->getCode() === self::HTTP_CODE_TOO_MANY_REQUESTS) {
                throw new TooManyRequestsException('Too many requests.', 0, $exception);
            }
            throw new HttpClientException('Bad response.', 0, $exception);
        } catch (RuntimeException $exception) {
            throw new HttpClientException('HttpClient runtime error.', 0, $exception);
        }

        return $responseString;
    }

    protected function initClientParams(): void
    {
        $cookieJar = new SessionCookieJar('SESSION_STORAGE', true);

        $this->params = [
            'headers' => [
                'User-Agent' => $this->userAgent ?: '',
            ],
            'allow_redirects' => true,
            'cookies' => $cookieJar,
        ];
    }

    protected function initClient(): void
    {
        $this->initClientParams();
        $this->client = new Client($this->params);
    }

    /**
     * @return Client
     */
    private function getClient(): Client
    {
        if ($this->client === null) {
            $this->initClient();
        }

        return $this->client;
    }
}
