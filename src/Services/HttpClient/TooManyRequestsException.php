<?php declare(strict_types=1);

namespace Client\Services\HttpClient;

use RuntimeException;

class TooManyRequestsException extends HttpClientException
{
}
