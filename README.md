﻿#### Добавляем обертку для метода

##### Класс запроса
Создаем класс запроса по аналогии с \Client\Api\Requests\Geos.
Если в запросе нужно передать параметры, добавляем в класс запроса свойство и сеттер. По аналоги с параметром \Client\Api\Requests\Geos::$type.
Метод \Client\Api\Requests\Geos::getData возвращает строку с параметрами запроса.

##### Менеджер
Добавляем имя класса запроса в массив \Client\Api\Manager::$methods

```
Geos::TYPE => [
    'method' => 'geo',
    'type' => 'continuous_get',
],
```
method - часть урла запроса, характеризующая метод (в апишке - это часть роута).
type - тип запроса

###### Типы запросов
get - обычный запрос типа GET
continuous_get - множественные запросы типа GET, в который с помощью параметров offset, count вытаскивается весь набор данных постранично. Данные возвращаются в сервис постепенно, с помощью генератора.
post - запрос типа POST

##### Коллекция или энтити ответа

Если ответ скалярный - возвращаем энтити. По аналогии с \Client\Api\Entities\Geo
Если в ответе массив данных, возвращаем коллекцию энтити. По аналогии с \Client\Api\Entities\Geos

В метод setData энтити или коллекции придет массив из ответа апишки.

##### Сервис
В классе Service создать метод, в который аттрибутами передаются параметры запроса. Этот метод возвращает энтити ответа.
По аналогии с \Client\Api\Service::getGeos.

#### Подключаем в проекте ларавель

В AppServiceProvider, в методе register биндим http клиент.
```
    $this->app->bind(HttpClient::class, function() {
        return new HttpClient();
    });
```
Там же - сервис для апишки. Параметры для апишки (хост, токен, юзер агент) берем из конфига.
```
    $this->app->bind(Service::class, function () {
        $httpClient = $this->app->make(Client::class);
        $httpClient->setUserAgent(config('api.user_agent'));

        $provider = new Provider($httpClient, config('api.token'), config('api.host'));
        $manager = new Manager($provider);

        return new Service($manager);
    });
```